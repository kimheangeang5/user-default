//
//  ViewController.swift
//  UserDefaut_Remember_Me
//
//  Created by Kimheang on 12/5/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func saveUserData(_ sender: Any) {
        if loginButton.titleLabel?.text == "Login" {
            showNameLabel.text = "Welcome " + userNameText.text!
//            UserDefaults.standard.set(userNameText.text, forKey: "userName")
//            UserDefaults.standard.set(passwordText.text, forKey: "password")
            UserDefaults.standard.set([["userName":userNameText.text],["password":passwordText.text]], forKey: "user")
            userNameText.text = userNameText.text
            passwordText.text = passwordText.text
            userNameText.isEnabled = false
            passwordText.isEnabled = false
            loginButton.setTitle("Logout", for: .normal)
        }
        else{
            showNameLabel.text = ""
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.synchronize()
            userNameText.text = ""
            passwordText.text = ""
            userNameText.isEnabled = true
            passwordText.isEnabled = true
            loginButton.setTitle("Login", for: .normal)
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if let user = UserDefaults.standard.object(forKey: "user") as? [[String:Any]] {
            showNameLabel.text = "Welcome \(user[0]["userName"]!)"
            userNameText.text = (user[0]["userName"] as! String)
            passwordText.text = (user[1]["password"] as! String)
            userNameText.isEnabled = false
            passwordText.isEnabled = false
            loginButton.setTitle("Logout", for: .normal)
        }
//        if let name = UserDefaults.standard.object(forKey: "userName") as? String,let pwd = UserDefaults.standard.object(forKey: "password") as? String {
//            showNameLabel.text = "Welcome " + name
//            userNameText.text = name
//            passwordText.text = pwd
//            userNameText.isEnabled = false
//            passwordText.isEnabled = false
//            loginButton.setTitle("Logout", for: .normal)
//        }
    }
    
}

